#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

PSYM buildtree(PSYM psym[], int N)
{
	int i;
	//������� ��������� ����
	PSYM temp=(PSYM)malloc(sizeof(TSYM));
	//� ���� ������� ������������ ����� ������ ���������� � �������������� ��������� ������� psym
	temp->freq=psym[N-2]->freq+psym[N-1]->freq;
	//��������� ��������� ���� � ����� ���������� ������
	temp->left=psym[N-1];
	temp->right=psym[N-2];
	temp->code[0]=0;
	if(N==2) //�� ������������ ������� ������� � �������� 1.0
		return temp; //����� �� ��������
	else
	{
		for(i=N-1;i>0;i--)
			if(temp->freq>psym[i-1]->freq)
			{
				psym[i]=psym[i-1];
				psym[i-1]=temp;
			}
		psym[N-1]=0;
	return buildtree(psym,N-1);
	}
}
void makeCodes(PSYM root)
{
	if(root->left)
	{
		strcpy(root->left->code,root->code);
		strcat(root->left->code,"0");
		makeCodes(root->left);
	}
	if(root->right)
	{
		strcpy(root->right->code,root->code);
		strcat(root->right->code,"1");
		makeCodes(root->right);
	}
}
char * decToBin(int x)
{
	char *t = (char *)malloc(100);
	int i = 0;
	char tmp;
	do
	{
		t[i++] = x % 2 + '0';
		x = x / 2;
	} while (i != 8);
	t[i] = '\0';
	return t;
}