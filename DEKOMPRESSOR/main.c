#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 256
#define P 4
#define T 11

int main(int argc, char* argv[])
{
	int i,j,kolsym,symff[N],hvost,len,lenstart,ch,flag=0,count=0;
	float fsymff[N];
	char id[P],newf[T]="Result",*t,*binar;
	PSYM root;
	PSYM psym[N];
	TSYM sym[N];
	TSYM tmp;
	FILE *fp;
	FILE *mp;
	fp=fopen(argv[1],"rb"); //��������� ���� ��� ������������
	if(!fp) //��������� ������� �� ����
	{
		perror("File: ");
		return 1;
	}
	for(i=0;i<P-1;i++)
		id[i]=fgetc(fp);
	id[i]=0;
	if(strcmp(id,"UR5")!=0)//�������� �������
	{
		puts("Files' id isn't right!");
		return 1;
	}
	kolsym=fgetc(fp);
	for(i=0;i<kolsym;i++)
	{
		symff[i]=fgetc(fp);
		fread(&fsymff[i],sizeof(float),1,fp);
	}
	for(i=0;i<kolsym;i++)//��������� ��������� ��� ������� �� ��������
		{
			sym[i].ch=(unsigned char)symff[i];
			sym[i].freq=fsymff[i];
			sym[i].code[0]=0;
			sym[i].left=0;
			sym[i].right=0;
		}
	for(i=0;i<kolsym;i++)//��������� ������ ���������� �� ���������
		psym[i]=&sym[i];

	root=buildtree(psym,kolsym);//������ ������
	makeCodes(root);//�������� ����

	hvost=fgetc(fp);
	lenstart=fgetc(fp);
	for(i=6;i<T-1;i++)
		newf[i]=fgetc(fp);
	newf[i]=0;

	mp=fopen("binar.txt","wb"); //��������� ���� ��� ������ ��������� ����
	if(!mp) //��������� ������� �� ����
	{
		perror("File: ");
		return 1;
	}

	while((ch=fgetc(fp))!=EOF)
	{
		t=decToBin(ch);
		fputs(t,mp);
	}
	fclose(mp);
	fclose(fp);

	mp=fopen("binar.txt","rb"); //��������� ���� ��������� ���� �� ������
	if(!mp) //��������� ������� �� ����
	{
		perror("File: ");
		return 1;
	}
	fseek(mp,0,SEEK_END);
	len=ftell(mp);//������ ������ mp
	rewind(mp);
	fp=fopen(newf,"wb"); //��������� ���� ��� ������ ����������� ������������
	if(!mp) //��������� ������� �� ����
	{
		perror("File: ");
		return 1;
	}
	binar=(char*)malloc(sizeof(char));
	i=0;
	while((ch=fgetc(mp))!=EOF)
	{
		count++;
		flag=0;
		binar[i]=ch;
		binar[i+1]=0;
		for(j=0;j<kolsym;j++)
			if(strcmp(binar,sym[j].code)==0)
			{
				flag=1;
				fputc(sym[j].ch,fp);
				binar[0]=0;
				i=0;
				break;
			}
		if(flag!=1)
			i++;
		if(count==len-(8-hvost))
			break;
	}

	return 0;
}