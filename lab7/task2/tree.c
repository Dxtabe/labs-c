#include "tree.h"
#include <stdio.h>
#include <stdlib.h>

#define N 500

char arrsym[N]={0};
int arrch[N]={0};

PNODE createTree(PNODE root,char sym)
{
	if(root==NULL)
	{
		root=(PNODE)malloc(sizeof(TNODE));
		root->sym=sym;
		root->count=1;
		root->left=root->right=NULL;
	}
	else if (root->sym>sym)
		 root->left=createTree(root->left,sym);
	else if (root->sym<sym)
		root->right=createTree(root->right,sym);
	else 
		root->count++;
	return root;
}
void makeArr(PNODE root)
{
	 static int i=0;
	 if(root->left)
		 makeArr(root->left);
	 arrsym[i]=root->sym;
	 arrch[i]=root->count;
	 i++;

	 if(root->right)
		 makeArr(root->right);
}
void sortandprintArr()
{
	char tmpsym=0;
	int tmpch=0,i,j;
	for(i=0;i<N;i++)
		for(j=0;j<N-1;j++)
			if(arrch[j]<arrch[j+1])
			{
				tmpch=arrch[j];
				arrch[j]=arrch[j+1];
				arrch[j+1]=tmpch;
				tmpsym=arrsym[j];
				arrsym[j]=arrsym[j+1];
				arrsym[j+1]=tmpsym;
			}
	for(i=0;i<N;i++)
		if(arrch[i]>0)
			printf("%c-%d\n",arrsym[i],arrch[i]);
}