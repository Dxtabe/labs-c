#ifndef _TREE_
#define _TREE_

struct NODE
{
char sym;
unsigned long count;
struct NODE *left;
struct NODE *right;
};
typedef struct NODE TNODE;
typedef TNODE* PNODE;
PNODE createTree(PNODE root,char sym);
void makeArr(PNODE root);
void sortandprintArr();
#endif