#include "tree.h"
#include <stdio.h>

int main(int argc,char **argv)
{
	FILE *fp;
	int ch;
	PNODE root=NULL; 
	fp=fopen(argv[1],"rt");
	if(!fp)
	{
		perror("File: ");
		return 1;
	}
	while((ch=fgetc(fp))!=EOF)
		root=createTree(root,(char)ch);
	fclose(fp);
	makeArr(root);
	sortandprintArr();
	return 0;
}
