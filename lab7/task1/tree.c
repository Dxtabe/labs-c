#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

PNODE makeTree(PNODE root,char *word)
{
	if(root==NULL)
	{
		root=(PNODE)malloc(sizeof(TNODE));
		strcpy(root->word,word);
		root->count=0;
		root->left=root->right=NULL;
	}
	else if (strcmp(root->word,word)>0)
		 root->left=makeTree(root->left,word);
	else if (strcmp(root->word,word)<0)
		root->right=makeTree(root->right,word);
	return root;
}
PNODE searchTree(PNODE root,char *ab)
{
		if(strcmp(root->word,ab)>0)
		{
			if(root->left==0)
				return root;
			searchTree(root->left,ab);			
		}
		else if(strcmp(root->word,ab)<0)
		{
			if(root->right==0)
				return root;
			searchTree(root->right,ab);			
		}
		else
			root->count++;
}
void printTree(PNODE root)
{
	if(root->left)
		 printTree(root->left);
	 printf("%s-%lu\n",root->word,root->count);

	 if(root->right)
		 printTree(root->right);
}