#include "tree.h"
#include <stdio.h>
#include <ctype.h>

int main(int argc,char **argv)
{
	FILE *fp;
	FILE *mp;
	char buf[100],analisbuf[100];
	int ch,i=0,j;
	PNODE root=NULL; 
	fp=fopen("key_words.txt","rt");
	if(!fp)
	{
		perror("File: ");
		return 1;
	}
	while(fscanf(fp,"%s",buf)==1)
		root=makeTree(root,buf);
	fclose(fp);

	mp=fopen(argv[1],"rt");
	if(!mp)
	{
		perror("File: ");
		return 1;
	}
	while((ch=fgetc(fp))!=EOF)
	{
		if(isalpha(ch))
		{
			analisbuf[i]=(char)ch;
			i++;
		}
		if(!isalpha(ch) && i>0)
		{
			analisbuf[i]=0;
			searchTree(root,analisbuf);
			for(j=0;j<100;j++)
				analisbuf[j]=0;
			i=0;
		}
	}
	fclose(mp);

	printTree(root);
	return 0;
}
