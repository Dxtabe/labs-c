#ifndef _TREE_
#define _TREE_

struct NODE
{
char word[100];
unsigned long count;
struct NODE *left;
struct NODE *right;
};
typedef struct NODE TNODE;
typedef TNODE* PNODE;
PNODE makeTree(PNODE root,char *word);
void printTree(PNODE root);
PNODE searchTree(PNODE root,char *ab);
#endif