#include "tree.h"
#include <stdio.h>
#include <string.h>

#define N 256
#define T 5

int main(int argc, char* argv[])
{
	int ch,i,j,t=0,count=0,ascii[N]={0},len,lenstart;
	unsigned char buf[8],rsym;
	char fname[T];
	float sum=0;
	PSYM root;
	PSYM psym[N];
	TSYM sym[N];
	TSYM tmp;
	FILE *fp;	
	FILE *mp;
	fp=fopen(argv[1],"rb"); //��������� ���� ��� ������	
	if(!fp) //��������� ������� �� ����
	{
		perror("File: ");
		return 1;
	}
	//strcpy(fname,argv[1]);
	fseek(fp,0,SEEK_END);
	lenstart=ftell(fp);//������ ������ ��������� �����
	rewind(fp);//�������� ������ ���� � ������
	while((ch=fgetc(fp))!=EOF)
		ascii[ch]++;//������� ������� ��� ���������� ������ ������ � �����
	for(i=0;i<N;i++)
		if(ascii[i]!=0)
			sum+=ascii[i];//������� ����� ������������� ���� ��������
	for(i=0;i<N;i++)//��������� ��������� ��� ������� �� ��������
		if(ascii[i]!=0)
		{
			sym[count].ch=(unsigned char)i;
			sym[count].freq=ascii[i]/sum;
			sym[count].code[0]=0;
			sym[count].left=0;
			sym[count].right=0;
			count++;
		}
	for(i=0;i<count;i++)//��������� ��������� �� �������� �� ������� �������������
		for(j=0;j<count-1;j++)
		{
			if(sym[j].freq<sym[j+1].freq)
			{
				tmp=sym[j];
				sym[j]=sym[j+1];
				sym[j+1]=tmp;
			}
		}
	for(i=0;i<count;i++)//��������� ������ ���������� �� ���������
		psym[i]=&sym[i];

	root=buildtree(psym,count);//������ ������
	makeCodes(root);//����������� ������� ������� ���

	rewind(fp);//�������� ������ ���� � ������
	mp=fopen("binar.txt","wb");//��������� ���� ��� ������ ����������� ����� fp � ���� ���������� �����
	if(!mp)
	{
		perror("File: ");
		return 1;
	}
	while((ch=fgetc(fp))!=EOF)
	{
		for(i=0;i<count;i++)
			if(sym[i].ch==(unsigned char)ch)
			{
				fputs(sym[i].code,mp); //������� ������ � �����
				break; //��������� �����
			}
	}
	fclose(fp);//��������� ���� fp
	fclose(mp);//��������� ���� mp

	mp=fopen("binar.txt","rb");//��������� �� ������ ���� ��������� � ������� �����
	if(!mp)
	{
		perror("File: ");
		return 1;
	}
	fseek(mp,0,SEEK_END);
	len=ftell(mp);//������ ������ mp
	fp=fopen("Result.rtxt","wb");//��������� ���� ��� ������ ���������� ������
	if(!fp)
	{
		perror("File: ");
		return 1;
	}

	fputs("UR5",fp);//������ ���������
	fputc(j+1,fp);
	for(i=0;i<=j;i++)
	{
		fputc(sym[i].ch,fp);
		fwrite(&sym[i].freq,sizeof(float),1,fp);
	}
	fputc(len%8,fp);
	fputc(lenstart,fp);
	for(i=0;i<N;i++)
		if(argv[1][i]=='.')
		{
			while(argv[1][i]!=0)
			{
				fname[t]=argv[1][i];
				t++;
				i++;
			}
		}
	fname[t]=0;
	fputs(fname,fp);//����� ���������
	
	rewind(mp);//�������� ������ ���� � ������
	j=0;
	do
	{
		j++;
		for(i=0;i<8;i++)//������ �� 8 0 � 1 �� ����� mp
		{
			ch=fgetc(mp);
			buf[i]=ch;
		}
		if(j>len/8)//��������� �����
			for(i=0;i<8;i++)
			{
				if(buf[i]!='0' && buf[i]!='1')
					buf[i]='0';
			}
		rsym=pack(buf);//������ ��������� ������������������ 1 � 0 � ������
		fputc(rsym,fp);//������� ������
	}while(ch!=EOF);
	fclose(fp);//��������� ���� fp
	fclose(mp);//��������� ���� mp
	return 0;
}