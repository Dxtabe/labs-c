#include <stdio.h>
#include <string.h>

#define N 30

int main()
{
	char buf[N][N]={0},*p=*buf;
	char *plines[N]={0};
	int i,j,count[N],tmp=0,countl=0;

	for(i=0; i<N; i++)
		count[i]=40;
	
	puts("Enter lines:");
	while(*fgets(p,N,stdin)!='\n')
	{
		plines[countl++]=p;
		p+=N;
	}
	buf[countl][0]=0;

	for(i=0; i<N; i++)
		for(j=0; j<N; j++)
		{
			while(buf[i][j]!=0)
			{
				count[i]++;
				j++;
			}
		}

	for(i=0; i<N; i++)
	{
		if (count[i]!=40)
			count[i]-=40;
	}

	for(i=0; i<N; i++)
		for(j=0; j<N-1; j++)
		{
			if(count[j]>count[j+1])
			{
				tmp=count[j];
				p=plines[j];
				count[j]=count[j+1];
				plines[j]=plines[j+1];
				count[j+1]=tmp;
				plines[j+1]=p;
			}
		}

	puts("Row in ascending order of length:");
	for(i=0; i<countl; i++)
		while(*plines[i]!=0)
			putchar(*plines[i]++);
	

	return 0;
}
