#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define N 20

void clean_stdin(void);

int main()
{
	char buf[N][N],*young,*old;
	int kolr=0,years[N],j,t,i=0,flag=0,maxy=0,miny=200,len;

	do
	{
		puts("Enter the number of family members:");
	    scanf("%d",&kolr);
		if(kolr==0)
			{
				puts("Input error!");
		        flag=1;
		        clean_stdin();
			}
			else
				flag=0;
	}while(flag>0);

	putchar('\n');

	while(kolr>0)
	{
		do
		{
			puts("Enter name:");	
	        scanf("%s",buf[i]);
			for (j=0; j<strlen(buf[i]); j++)
				if(isdigit(buf[i][j]) || ispunct(buf[i][j]) || isspace(buf[i][j]))
				{
					puts("Input error!");
		            flag=1;
		            clean_stdin();
					break;
				}
				else
					flag=0;
		}while(flag>0);
		buf[i][strlen(buf[i])+1]=0;

		do
		{
			puts("Enter age:");
			scanf("%d",&years[i]);
			if(years[i]==0)
			{
				puts("Input error!");
		        flag=1;
		        clean_stdin();
			}
			else
				flag=0;
		}while(flag>0);

		if(years[i]<miny)
		{
			young=buf[i];
			miny=years[i];
		}

		if(years[i]>maxy)
		{
			old=buf[i];
			maxy=years[i];
		}
		
		putchar('\n');
		i++;
		kolr--;
	}

	puts("The youngest relative is:");
	while(*young)
		putchar(*young++);
	putchar('\n');
	puts("The oldest relative is:");
	while(*old)
		putchar(*old++);
	putchar('\n');

	return 0;
}

void clean_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}