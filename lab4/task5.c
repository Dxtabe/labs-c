#include <stdio.h>
#include <string.h>

#define N 30

int main()
{
	char buf[N][N]={0},*p=*buf;
	char *plines[N]={0};
	int i=0,j,count[N],tmp=0,countl=0;

	FILE *fp;
	FILE *mf;
	fp=fopen("1.txt","r");
	if(fp==0)
	{
		puts("Error!");
		return 1;//exit(1);
	}
	mf=fopen("2.txt","w");
	if(mf==0)
	{
		puts("Error!");
		return 1;//exit(1);
	}

	for(i=0; i<N; i++)
		count[i]=40;
	
	i=0;
	while(feof(fp)==0)
	{
		p=fgets(buf[countl],sizeof(buf),fp);
		plines[i]=p;
		countl++;
		i++;
	}

	fclose(fp);

	for(i=0; i<N; i++)
		for(j=0; j<N; j++)
		{
			while(buf[i][j]!=0)
			{
				count[i]++;
				j++;
			}
		}

	for(i=0; i<N; i++)
	{
		if (count[i]!=40)
			count[i]-=40;
	}

	for(i=0; i<N; i++)
		for(j=0; j<N-1; j++)
		{
			if(count[j]>count[j+1])
			{
				tmp=count[j];
				p=plines[j];
				count[j]=count[j+1];
				plines[j]=plines[j+1];
				count[j+1]=tmp;
				plines[j+1]=p;
			}
		}

	for(i=0; i<countl-1; i++)
		while(*plines[i]!=0)
			fputc(*plines[i]++,mf);
	fclose(mf);

	return 0;
}
