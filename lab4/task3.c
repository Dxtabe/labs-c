#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main()
{
	char buf[80],*ps,*pe;
	int len,flag=0;
	puts("Enter a line:");
	fgets(buf,80,stdin);
	buf[strlen(buf)-1]=0;
	ps=buf;
	len=strlen(buf);
	pe=buf+len-1;

	while(*ps)
	{
		if (isalpha(*ps) && isalpha(*pe))
		{
			if(*ps!=*pe)
		    {
			puts("It isn't polindrom.");
			flag=1;
			break;
		    }
			ps++;
		    pe--;
		}
		else if (isalpha(*ps) && !isalpha(*pe))
			pe--;
		else if (!isalpha(*ps) && isalpha(*pe))
			ps++;
		else if (!isalpha(*ps) && !isalpha(*pe))
		{
			ps++;
			pe--;
		}		
	}

	if (flag==0)
		puts("It is polindrom!");

	return 0;
}
