#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 80

int getWords(char *arr,char **pointer);
void printWord(char **pointer);

int main()
{
	char buf[N],*p[N],*s,numbrand[N]={0};
	int count=0,i,random,len;
	srand(time(0));
	puts("Enter string:");
	fgets(buf,N,stdin);
	len=strlen(buf);
	if(buf[len-1]=='\n')
		buf[len-- -1]=' ';
	getWords(buf,p);
	count=getWords(buf,p);
	for(i=0;i<count;i++)
	{
		do
		{
		random=rand()%count;
		}while(numbrand[random]>0);
		numbrand[random]++;
		s=p[random];
		printWord(s);
		putchar(' ');
	}
	return 0;
}

int getWords(char *arr,char **pointer)
{
	char *s;
	int inWord=0;
	int counter=0;
	s=arr;
	while(*s)
	{
		if (*s!=' ' && inWord==0)
		{
			inWord=1;
			*pointer=s;
			pointer++;
			counter++;
		}
		else if (*s==' '  && inWord==1)
			inWord=0;		
		s++;
	}
	return counter;
}

void printWord(char *s)
{	
	while(*s!=' ')
	{
		putchar(*s);
		s++;
	}
}