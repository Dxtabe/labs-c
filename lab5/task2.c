#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

#define W 9

void clean(char (*arr)[W]);
void createhl(char (*arr)[W]);
void copyhl(char (*arr)[W]);
void print(char (*arr)[W]);

int main()
{
	char square[W][W];	
	srand(time(0));
	for(;;)
	{
		clean(square);
		createhl(square);
		copyhl(square);
		print(square);
		Sleep(1000);
		system("cls");
	}
    return 0;
}

void clean(char (*arr)[W])
{
	int i,j;	
	for (i=0; i<=W; i++)
		for(j=0; j<=W; j++)
			arr[i][j]=' ';
}

void createhl(char (*arr)[W])
{
	int i,j,randomc;
	for(i=0; i<=W/2; i++)
		for(j=0; j<=W/2; j++)
		{
			randomc=rand()%2;
			if (randomc==1)
				arr[i][j]='*';
		}
}

void copyhl(char (*arr)[W])
{
	int i,j;
	for(i=0; i<=W; i++)
		for(j=0; j<=W; j++)
			if (arr[i][j]=='*')
				arr[i][W-j]='*';

	for(j=0; j<=W; j++)
		for(i=0; i<=W; i++)
			if (arr[i][j]=='*')
				arr[W-i][j]='*';
}

void print(char (*arr)[W])
{
	int i,j;
	for(i=0; i<=W; i++)
	{
		putchar('\n');
		for(j=0; j<=W; j++)
			putchar(arr[i][j]);
	}
}