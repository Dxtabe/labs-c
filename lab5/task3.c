#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 80

int countw(char *arr);
int mix_and_print(char *arr);

int main()
{
	char buf[N]={0},*p=buf,word[N]={0},*s;
	int i,len,lenm=0,count=0;
	FILE *fp;
	srand(time(0));
	fp=fopen("1.txt","r");
	
	while(feof(fp)==0)
	{
		p=fgets(buf,sizeof(buf),fp);
		len=strlen(buf);
		if(buf[len-1]=='\n')
			buf[len-- -1]=' ';
		count=countw(buf);
		for(i=0;i<count;i++)
		{
			s=buf+lenm+i;			
			lenm+=mix_and_print(s);
			putchar(' ');
		}
		putchar('\n');
		buf[0]=0;
		lenm=0;
	}
	return 0;
}

int countw(char *arr)
{
	char *s;
	int inWord=0;
	int counter=0;
	s=arr;
	while(*s)
	{
		if (*s!=' ' && inWord==0)
		{
			inWord=1;
			counter++;
		}
		else if (*s==' '  && inWord==1)
			inWord=0;		
		s++;
	}
	return counter;
}

int mix_and_print(char *s)
{
	int i=0,lenm,random,randoms=0;
	char arr[N],tmp;
	while(*s!=' ')
	{
		arr[i]=*s;
		s++;
		i++;
	}
	arr[i]=0;
	lenm=strlen(arr);
	for(i=1; i<lenm-1; i++)
	{
		if (lenm>3)
			do
			{
				random=rand()%(lenm-2)+1;
			}while(random==i || random==randoms);
		else
			continue;
		tmp=arr[random];
		arr[random]=arr[i];
		arr[i]=tmp;
		randoms=random;
		if (lenm==4)
			i++;
	}
	printf("%s",arr);
	return lenm;
}