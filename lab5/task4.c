#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 80

int getWords(char *arr,char **pointer);
void printWord(char **pointer);

int main()
{
	char buf[N]={0},*p=buf,*point[N],*s;
	int countl=0,len,random,count=0,i,numbrand[N]={0};
	FILE *fp;
	
	srand(time(0));
	
	fp=fopen("1.txt","r");
	if (fp==0)
	{
		perror("File wasn't open because:");
		exit(1);
	}

	while(feof(fp)==0)
	{
		p=fgets(buf,sizeof(buf),fp);
		len=strlen(buf);
		if(buf[len-1]=='\n')
			buf[len-- -1]=' ';
		getWords(buf,point);
		count=getWords(buf,point);
	    for(i=0;i<count;i++)
		{
			do
			{
				random=rand()%count;
			}while(numbrand[random]>0);
			numbrand[random]++;
		    s=point[random];
		    printWord(s);
		    putchar(' ');
	    }
		putchar('\n');
		for(i=0;i<N;i++)
			numbrand[i]=0;
		buf[0]=0;
		countl++;
	}
	return 0;
}

int getWords(char *arr,char **pointer)
{
	char *s;
	int inWord=0;
	int counter=0;
	s=arr;
	while(*s)
	{
		if (*s!=' ' && inWord==0)
		{
			inWord=1;
			*pointer=s;
			pointer++;
			counter++;
		}
		else if (*s==' '  && inWord==1)
			inWord=0;		
		s++;
	}
	return counter;
}

void printWord(char *s)
{	
	while(*s!=' ')
	{
		putchar(*s);
		s++;
	}
}