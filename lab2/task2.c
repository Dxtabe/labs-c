#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

void clean_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int main()
{
	int progch, polzch,flag;
	srand(time(NULL));
	progch=rand()%100+1;
	flag=0;
		
	puts("Guess the number!");
		
	do
	{

		do
		{
			puts("Enter your variant:");
	        scanf("%d",&polzch);
			if(isalpha(polzch) || polzch<0)			
	        {	
		        puts("Input error!");
		        flag=1;
		        clean_stdin();
	        }
	        else
		        flag=0;
		}
		while(flag>0);

	if(polzch>progch)
		puts("The secret number is less");
	if(polzch<progch)
		puts("The secret number is bigger");
	}
	while(polzch!=progch);

	puts("You are right!");
	return 0;
}