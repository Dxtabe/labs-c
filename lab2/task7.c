#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main()
{
	char str[50];
	int ascii[128]={0};
	int i;
	puts("Enter the string:");
	fgets(str,50,stdin);
	str[strlen(str)-1]=0;

	for(i=0; i<strlen(str); i++)
	{
		ascii[str[i]]++;
	}

	for(i=0; i<128; i++)
	{
		if(ascii[i])
		{
			printf("%c\t%d\n",i,ascii[i]);
		}

	}

	return 0;
}