#include <stdio.h>
#include <ctype.h>

void clean_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int main()
{
	double H,L,h;
	int t=0;
	double g=9.81;
	int flag=0;

	do
	{	
	puts("Enter height:");	
	scanf("%lf",&H);

	if(isalpha(H) || H<0)
	{	
		puts("Input error!");
		flag=1;
		clean_stdin();
	}
	else
		flag=0;
	} while(flag>0);
	
	L=0;
	h=H;

	while(h>0)
	{
		L=(g*t*t)/2;
		h=H-L;
		if(h<0)
		{
			puts("BABAH!");
			break;
		}
		printf("t=%.1d c\t",t);
		printf("h=%.1lf m\n",h);
		t++;
	}
	
return 0;
}