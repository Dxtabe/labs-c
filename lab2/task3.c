#include <stdio.h>
#include <string.h>
#include <ctype.h>

void clean_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int main()
{
	char str[100];
	int kolstr,i,flag=0;	

	do
		{
			puts("Enter the number of rows:");
	        scanf("%d",&kolstr);
			if(isalpha(kolstr) || kolstr<0)			
	        {	
		        puts("Input error!");
		        flag=1;
		        clean_stdin();
	        }
	        else
		        flag=0;
		}
		while(flag>0);

	i=kolstr;

	puts("Triangle:");

	memset(str, ' ', kolstr);
	str[kolstr-1]='*';
	str[i]='\0';
	while(kolstr!=0)
	{	
		if(i!=kolstr)
		{
			str[kolstr-1]='*';
		    strcat(str,"*");		
		    str[i+1]='\0';
		}
		puts(str);
		i++;
		kolstr--;
	}
	return 0;
}