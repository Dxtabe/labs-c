#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int sum_by_recursion(int m[], int l, int r);

int main()
{
	int N,M=25,num,i,sum=0,sumr=0;
	double start,stop;
	int *arr;
	srand(time(0));
	N=pow(2.0,M);
	arr=(int*)malloc(sizeof(int)*N);
	for(i = 0;i < N;i++)
	{
		num = rand() % 99 + 1;
		arr[i] = num;
	}
	start=clock();
	for(i = 0;i < N;i++)
	{
		sum+=arr[i];
	}
	stop=clock();	
	printf("Sum: %d\nTime: %.3f\n",sum,(stop-start)/CLOCKS_PER_SEC);
	start=clock();
	sumr=sum_by_recursion(arr,0,N-1);
	stop=clock();
	putchar('\n');
	printf("Sum by rekursion: %d\nTime: %.3f\n",sumr,(stop-start)/CLOCKS_PER_SEC);
	free(arr);
	return 0;
}

int sum_by_recursion(int *arr, int l, int r)
{
	int m=(l+r)/2;
	if(l==r)
		return arr[l];
	else
		return sum_by_recursion(arr,l,m)+sum_by_recursion(arr,m+1,r);
}