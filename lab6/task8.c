#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define N 80

int eval(char *buf);
char partition(char *buf, char *expr1, char *expr2);
void clean_stdin(void);

int main()
{
	int i,flag=0,len;
	char buf[N]={0};
	puts("Enter mathematic expression(you can use: 0-9,+,-,*,/,(,)");
	do
	{
		scanf("%s",buf);
		len=strlen(buf);
		for(i=0;i<len-1;i++)
			if (!isdigit(buf[i]) && buf[i]!='+' && buf[i]!='-' && buf[i]!='*' && buf[i]!='/' && buf[i]!='(' && buf[i]!=')')
			{
				puts("Input error!\nPlease,enter right expression:");
				flag=1;
				clean_stdin();
				break;
			}
			else
				flag=0;
	}while(flag==1);
	printf("\nResult: %d\n",eval(buf));
	return 0;
}

int eval(char *buf)
{
	char s1[N]={0},s2[N]={0},operation;
	if (buf[0]!='(')
		return atoi(buf);
	else
		operation=partition(buf,s1,s2);
	switch (operation)
	{
	case '+':
		return eval(s1)+eval(s2);
	case '-':
		return eval(s1)-eval(s2);
	case '*':
		return eval(s1)*eval(s2);
	case '/':
		return eval(s1)/eval(s2);
	}
}

char partition(char *buf, char *expr1, char *expr2)
{
	char operation;
	int i=0,len,inbrackets=0,clbrack=0,crbrack=0;
	len=strlen(buf);
	if(buf[len-1]==')')
	{
		while(i!=len)
		{
			buf[i]=buf[i+1];
			i++;
		}
		buf[len-2]=0;
	}
	while(*buf)
	{
		if(*buf=='(')
		{
			clbrack++;
		}
		if(*buf==')')
		{
			crbrack++;
		}
		if(clbrack-crbrack==0)
		{
			*expr1=*buf;
			operation=*(buf+1);
			break;
		}
		*expr1=*buf;
		expr1++;
		buf++;
	}
	buf=buf+2;
	while(*buf)
	{
		*expr2=*buf;
		buf++;
		expr2++;
	}
	return operation;
}

void clean_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}