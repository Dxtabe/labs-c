#include <stdio.h>

#define N 80

int make_string_by_number(int numb, int poz);

char string[N]={0};

int main()
{
	int numb,numbcopy,count=0;	
	puts("Enter number:");
	scanf("%d",&numb);
	numbcopy=numb;
	while(numbcopy!=0)
	{
		numbcopy=numbcopy/10;
		count++;
	}
	make_string_by_number(numb,count-1);
	puts("\nString:");
	printf("%s\n",string);
	return 0;
}

int make_string_by_number(int numb, int poz)
{
	int numeral;
	static int numbwon=0;
	numeral=numb%10;
	numbwon=numb/10;
	while(numeral!=0)
	{
		string[poz]=numeral+'0';
		poz--;
		return make_string_by_number(numbwon,poz);
	}
}