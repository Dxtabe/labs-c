#include <stdio.h>

#define N 27

char fraktal[N][N];
char fraktal_part[N][N];

void fraktal_creation(int x, int y);

int main()
{
	static int x=13,y=13,i,j;
	for(i=0;i<N;i++)
		for(j=0;j<N;j++)
			fraktal[i][j]=' ';
	fraktal_creation(x,y);
	for(i=0;i<N;i++)
	{
		for(j=0;j<N;j++)
			putchar(fraktal[i][j]);
		putchar('\n');
	}
	return 0;
}

void fraktal_creation(int x, int y)
{
	int x1,y1;
	static int count=0,i=0,t,j,c=1;
	fraktal[x][y]='*';
	if (count!=0 && count%4==0)
	{
		x=x-c;
		for(t=0;t<N;t++)
			for(j=0;j<N;j++)
				fraktal_part[t][j]=fraktal[t][j];
		c=c*3;
		count=0;		
	}
	count++;	
	if(count==1)
	{
		x1=x;
		y1=y-c;
		if(c>=3)
			for(t=0;t<N;t++)
				for(j=0;j<N;j++)
					if(fraktal_part[t][j]=='*')
						fraktal[t][j-c]=fraktal_part[t][j];
	}
	if(count==2)
	{
		x1=x-c;
		y1=y+c;
		if(c>=3)
			for(t=0;t<N;t++)
				for(j=0;j<N;j++)
					if(fraktal_part[t][j]=='*')
						fraktal[t-c][j]=fraktal_part[t][j];
	}
	if(count==3)
	{
		x1=x+c;
		y1=y+c;
		if(c>=3)
			for(t=0;t<N;t++)
				for(j=0;j<N;j++)
					if(fraktal_part[t][j]=='*')
						fraktal[t][j+c]=fraktal_part[t][j];
	}
	if(count==4)
	{
		x1=x+c;
		y1=y-c;
		if(c>=3)
			for(t=0;t<N;t++)
				for(j=0;j<N;j++)
					if(fraktal_part[t][j]=='*')
						fraktal[t+c][j]=fraktal_part[t][j];
	}	
	i++;
	while(i<12)
		fraktal_creation(x1, y1);
}