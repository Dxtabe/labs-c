#include <stdio.h>
#include <time.h>

unsigned long fib(int n)
{
	if(n==1 || n==2)
		return 1;
	else
		return fib(n-1)+fib(n-2);
}

int main()
{
	int i,j;
	double	start,stop,time;
	unsigned long tmp;
	FILE *fp;
	fp=fopen("1.txt","w");
	for(i=1;i<=40;i++)
	{
		start=clock();
		tmp=fib(i);		
		stop=clock();
		printf("%ld\t",tmp);
		fprintf(fp,"%ld\t",tmp);
		if (i<36)//��� ������������
		{
			putchar('\t\t');
			fputc('\t\t',fp);
		}
		if (i<17)//��� ������������
			fputc('\t\t\t',fp);
		time=(stop-start)/CLOCKS_PER_SEC;
		printf("%.1f sec.\n",time);
		fprintf(fp,"%.1f sec.\n",time);		
	}
	fclose(fp);
	return 0;
}