#include <stdio.h>

typedef unsigned long UL;

UL kollatc(unsigned int n, unsigned int count)
{
	count++;
	if(n==1)
		return count;
	else if(n%2==0)
		return kollatc(n/2,count);
	else
		return kollatc(3*n+1,count);
}

int main()
{
	unsigned int i,len,maxlen=0,snumb,count;
	for(i=2; i<=1000000; i++)
	{
		len=kollatc(i,count=0);
		if(len>maxlen)
		{
			maxlen=len;
			snumb=i;
		}
	}
	printf("%d\t%d\n",snumb,maxlen);
	return 0;
}

