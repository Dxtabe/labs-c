#include <stdio.h>
#include <windows.h>

#define N 30
#define S 300

char labirint[N][N]={0};

void full_labirint(char (*labirint)[N]);
void reprint(char (*labirint)[N]);
int step(int x, int y);

int main()
{
	int i=0,x=4,y=13;
	full_labirint(labirint);
	labirint[x][y]='x';
	for (i=0;i<N;i++)
		printf("%s",labirint[i]);
	step(x,y);
	return 0;
}

void full_labirint(char (*labirint)[N])
{
	int i=0;
	FILE *fp;
	fp=fopen("1.txt","r");
	while(feof(fp)==0)
	{
		fgets(labirint[i],sizeof(labirint[i]),fp);
	    i++;
	}
}

void reprint(char (*labirint)[N])
{
	int i;
	Sleep(S);
	system("cls");
	for (i=0;i<N;i++)
		printf("%s",labirint[i]);
}

int step(int x, int y)
{
	int i;
	if(labirint[x][y-1]=='O' || labirint[x-1][y]=='O' || labirint[x][y+1]=='O' || labirint[x+1][y]=='O')
	{
		labirint[x][y]='.';				
		reprint(labirint);
	    puts("\nCongratulation!\nYou came out of the labirint!\n");
		exit (0);
	}
	labirint[x][y]='.';
	if(labirint[x][y-1]==' ')
	{
		labirint[x][y-1]='x';		
		reprint(labirint);
		step(x,y-1);//�����
	}
	if(labirint[x-1][y]==' ')
	{
		labirint[x-1][y]='x';
		reprint(labirint);
		step(x-1,y);//�����
	}
	if(labirint[x][y+1]==' ')
	{
		labirint[x][y+1]='x';
		reprint(labirint);
		step(x,y+1);//������
	}
	if(labirint[x+1][y]==' ')
	{
		labirint[x+1][y]='x';
		reprint(labirint);
		step(x+1,y);//����
	}
}