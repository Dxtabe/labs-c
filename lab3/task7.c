#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main()
{
	char str[50],dvstr[50][2],tmp,tmp1;
	int ascii[128]={0};
	int i,j,len,s=0;
	puts("Enter the string:");
	fgets(str,50,stdin);
	str[strlen(str)-1]=0;

	for(i=0; i<strlen(str); i++)
	{
		ascii[str[i]]++;
	}

	for(i=0; i<128; i++)
		if(ascii[i]!=0)
		{
			dvstr[s][0]=i;
			dvstr[s][1]=ascii[i];
			s++;
		}

	dvstr[s][0]=0;	
			
	for(i=0; i<s-1; i++)
	{
      for(j=0; j<s-i-1; j++) 
	  { 
		  if(dvstr[j][1] < dvstr[j+1][1]) 
		  {
             tmp = dvstr[j][0];
			 tmp1 = dvstr[j][1];
			 dvstr[j][0] = dvstr[j+1][0];
			 dvstr[j][1] = dvstr[j+1][1];
			 dvstr[j+1][0] = tmp;
			 dvstr[j+1][1] = tmp1;
          }
	  }
	}
	
	len=strlen(str);

	for(i=0; i<s; i++)
	{
		printf("%c\t%d\n",dvstr[i][0],dvstr[i][1]);
	}

	return 0;
}