#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main()
{
	int buf[10],sum=0;
	int i,min=51,max=0,pozmin=0,pozmax=0;
	srand(time(0));

	for(i=0;i<10;i++)	
		buf[i]=rand()%50+1;
	
	for(i=0;i<10;i++)
	{
		if(buf[i]<min)
		{
			min=buf[i];
			pozmin=i;
		}
		if(buf[i]>max)
		{
			max=buf[i];
			pozmax=i;
		}		
	}
	
	if(pozmax>pozmin)
		for(i=pozmin+1;i<pozmax;i++)
			sum+=buf[i];

	if(pozmax<pozmin)
		for(i=pozmax+1;i<pozmin;i++)
			sum+=buf[i];

	printf("%d\n",sum);
	
	return 0;
}