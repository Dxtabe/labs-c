#include <stdio.h>
#include <string.h>
 
#define MAX_LEN 80
 
char *middle(char *str){
    static char buf[MAX_LEN + 1];
    size_t spaces_needed, str_len;
    
    str_len = strlen(str);
    if ( str_len >= MAX_LEN )
        return str;
    
    spaces_needed = (MAX_LEN - str_len) / 2;
    memset(buf, ' ', spaces_needed);
    strcat(buf + spaces_needed, str);
    
    return buf;
}
 
int main(void){
    char str[100];
	puts("Enter the string:");
	scanf("%s",str);
    
	puts("A centered string:");
    printf("%s\n", middle(str));
    
    return 0;
}