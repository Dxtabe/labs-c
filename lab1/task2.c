#include <stdio.h>

int main()
{
	int hours,minutes,seconds;

	puts("Enter the time in HH:MM:SS");	
	if(scanf("%d:%d:%d",&hours,&minutes,&seconds)==0)
	{
		puts("Input error!");
		return 1;
	}

	if(hours<0 || hours>24 || minutes<0 || minutes>60 || seconds<0 || seconds>60)
	{
		puts("Input error!");
		return 1;
	}

	if(hours>=4 && hours<=11)
	{
		puts("Good morning!");
	}

	if(hours>=12 && hours<=16)
	{
		puts("Good afternoon!");
	}

	if(hours>=17 && hours<=23)
	{
		puts("Good evening!");
	}

	if(hours>=0 && hours<=3)
	{
		puts("Good night!");
	}

	return 0;
}