#include <stdio.h>

int main()
{
	int ft, ins;
	float sm;

	puts("Enter ft:");
	if(scanf("%d",&ft)==0)
	{
		puts("Input error!");
		return 1;
	}

	puts("Enter inches:");
	if(scanf("%d",&ins)==0)
	{
		puts("Input error!");
		return 1;
	}

	sm=(ft*12+ins)*2.54;
	printf("%.1f",sm);

	return 0;
}