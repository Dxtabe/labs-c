#include <stdio.h>

int main()
{
	char units;
	float angle;

	puts("Enter angle (for example 45.00D(D-degrees,R-radians)):");

	if(scanf("%f%c",&angle,&units)==0)
	{
		puts("Input error!");
		return 1;
	}

	switch (units)
	{
	case 'R':
		angle=angle*0.0174533;
		printf("%f",angle);

	case 'D':
		angle=angle*57.2958;
		printf("%f",angle);
	}

	return 0;
}