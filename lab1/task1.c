#include <stdio.h>

int main()
{
	int sex;
	double height, weight, index, height2;

	puts("Enter your sex:");
	puts("1-man, 2-woman");
	if(scanf("%d",&sex)==0)
	{
		puts("Input error!");
		return 1;
	}
	if(sex<1 || sex>2)
	{
		puts("Input error!");
		return 1;
	}

	puts("Enter your height:");
	if(scanf("%lf",&height)==0)
	{
		puts("Input error!");
		return 1;
	}
	if(height<140 || height>225)
	{
		puts("Input error!");
		return 1;
	}

	puts("Enter your weight:");
	if(scanf("%lf",&weight)==0)
	{
		puts("Input error!");
		return 1;
	}
	if(weight<35 || weight>160)
	{
		puts("Input error!");
		return 1;
	}

	height2=height/100;
	index=weight/(height2*height2);

	switch (sex)
	{
	case 1:
		{
			if (index>=19 && index<=25)
				puts("You're in good shape!");
			else if (index<19)
				puts("You need to gain weight!");
			else if (index>25)
				puts("You need to lose weight!");
			break;
		}
	case 2:
		{
			if (index>=19 && index<=24)
				puts("You're in good shape!");
			else if (index<19)
				puts("You need to gain weight!");
			else if (index>24)
				puts("You need to lose weight!");
		}
	}	
	return 0;
}